﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemZaRezervacijeApartmana.Models
{
    public class Lokacija
    {
        public double GeografskaSirina { get; set; }
        public double GeografskaDuzina { get; set; }
        public Adresa Adresa { get; set; }
    }
}