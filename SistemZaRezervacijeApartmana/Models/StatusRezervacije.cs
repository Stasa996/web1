﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemZaRezervacijeApartmana.Models
{
    public enum StatusRezervacije
    {
        KREIRANA,
        ODBIJENA,
        PRIHVACENA,
        ODUSTANAK,
        ZAVRSENA
        
    }
}