﻿using SistemZaRezervacijeApartmana.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SistemZaRezervacijeApartmana
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

           
            string tmp = "";
            string[] line;
            Pol pol;

            string path = "~/App_Data/Admini.txt";
            path = HostingEnvironment.MapPath(path);

            if (File.Exists(path))
            {
                StreamReader sr = new StreamReader(path);
                while ((tmp = sr.ReadLine()) != null)
                {
                    line = tmp.Split('_');
                    if (line[4] == "MUSKI")
                        pol = Pol.MUSKI;
                    else
                        pol = Pol.ZENSKI;

                    Admin admin = new Admin(line[0], line[1], line[2], line[3], pol);
                    Database.registrovaniKorisnici.Add(line[2], admin);
                }

                sr.Close();   
            }

            

            UcitajBazu();
        }

        public void UcitajBazu()
        {
            string temp = "";
            string[] read;

            string path = "~/App_Data/RegistrovaniKorisnici.txt";
            path = HostingEnvironment.MapPath(path);

            if (File.Exists(path))
            {
                StreamReader srUser = new StreamReader(path);
                while ((temp = srUser.ReadLine()) != "")
                {
                    read = temp.Split('_');
                    Pol pol;

                    if (read[4] == "MUSKI")
                        pol = Pol.MUSKI;
                    else
                        pol = Pol.ZENSKI;

                    Uloga uloga;

                    if (read[5] == "GOST")
                        uloga = Uloga.GOST;
                    else if (read[5] == "DOMACIN")
                        uloga = Uloga.DOMACIN;
                    else
                        uloga = Uloga.ADMINISTRATOR;

                    Korisnik k = new Korisnik(read[0], read[1], read[2], read[3], pol);
                    k.Uloga = uloga;

                    if (!Database.registrovaniKorisnici.ContainsKey(read[2]))
                    {
                        Database.registrovaniKorisnici.Add(k.KorisnickoIme, k);
                    }
                }

                srUser.Close();
            }


            string path1 = "~/App_Data/Domacini.txt";
            path1 = HostingEnvironment.MapPath(path1);


            if(File.Exists(path1))
            {
                StreamReader sr = new StreamReader(path1);

                while((temp=sr.ReadLine())!="")
                {
                    read = temp.Split('_');

                    Pol p;
                    if (read[4] == "MUSKI")
                        p = Pol.MUSKI;

                    else
                        p = Pol.ZENSKI;

                    Domacin d = new Domacin(read[0],read[1],read[2],read[3],p);

                    if(!Database.domacini.ContainsKey(read[0]))
                    {
                        Database.domacini.Add(read[2], d);
                    }

                }

                sr.Close();
            }


            string pathApartmani = "~/App_Data/SviApartmani.txt";
            pathApartmani = HostingEnvironment.MapPath(pathApartmani);
            Random idAprtman = new Random();
            int idApr; 

            if (File.Exists(pathApartmani))
            {
                StreamReader sr = new StreamReader(pathApartmani);

                while ((temp = sr.ReadLine()) != null)
                {
                    Apartman apartman = new Apartman();

                    read = temp.Split('|');

                    idApr = idAprtman.Next();

                    if(read.Count() > 1)
                    {
                        if (read[0] == "SOBA")
                        {
                            apartman.TipApartman = TipApartmana.SOBA;
                        }
                        else
                        {
                            apartman.TipApartman = TipApartmana.CEO_APARTMAN;
                        }

                        apartman.BrojSoba = Int32.Parse(read[1]);
                        apartman.BrojGostiju = Int32.Parse(read[2]);
                        apartman.CenaPoNoci = Int32.Parse(read[3]);

                        Adresa adresa = new Adresa();

                        adresa.Ulica = read[4];
                        adresa.Broj = read[5];
                        adresa.PostanskiBroj = read[6];
                        adresa.Grad = read[7];

                        Lokacija lokacija = new Lokacija();

                        lokacija.Adresa = adresa;

                        Domacin domacin = new Domacin();

                        domacin.Ime = read[8];
                        domacin.Prezime = read[9];

                        apartman.Domacin = domacin;

                        apartman.VremeZaPrijavu = read[10];
                        apartman.VremeZaOdjavu = read[11];

                        if (read[12] == "AKTIVAN")
                        {
                            apartman.Status = StatusApartmana.AKTIVAN;
                        }
                        else
                        {
                            apartman.Status = StatusApartmana.NEAKTIVAN;
                        }

                        if(read[13] == "0")
                        {
                            apartman.IsDeleted = true;
                        }
                        else
                        {
                            apartman.IsDeleted = false;
                        }

                        if (!Database.sviApartmani.ContainsKey(idApr))
                        {
                            Database.sviApartmani.Add(idApr, apartman);
                        }

                    }

                }

                sr.Close();
            }

            string path2 = "~/App_Data/KomentariZaApartmane.txt";
            path2 = HostingEnvironment.MapPath(path2);
            Random id = new Random();
           // Random idAp = new Random();
            int idKom;

            if (File.Exists(path2))
            {
                StreamReader sr = new StreamReader(path2);

                while ((temp = sr.ReadLine()) != null)
                {
                    KomentarZaApartman komentar = new KomentarZaApartman();

                    read = temp.Split('|');

                    if(read.Count() > 1)
                    {
                        idKom = id.Next();

                        Gost gost = new Gost();
                        gost.KorisnickoIme = read[0];

                        komentar.Gost = gost;

                        Apartman apartman = new Apartman();

                        var lok = read[1].Split('_',',');

                        Adresa adresa = new Adresa();
                        adresa.Ulica = lok[0];
                        adresa.Broj = lok[1];
                        adresa.Grad = lok[2];

                        Lokacija lokacija = new Lokacija();

                        lokacija.Adresa = adresa;

                        apartman.Lokacija = lokacija;

                        komentar.Apartman = apartman;

                        OcenaApartmana ocena = new OcenaApartmana();

                        if (read[3].ToUpper() == "PET")
                            ocena = OcenaApartmana.PET;
                        else if (read[3].ToUpper() == "CETIRI")
                            ocena = OcenaApartmana.ČETIRI;
                        else if (read[3].ToUpper() == "TRI")
                            ocena = OcenaApartmana.TRI;
                        else if (read[3].ToUpper() == "DVA")
                            ocena = OcenaApartmana.DVA;
                        else if (read[3].ToUpper() == "JEDAN")
                            ocena = OcenaApartmana.JEDAN;
                        else
                            ocena = OcenaApartmana.NULA;

                        komentar.OcenaApartmana = ocena;

                        komentar.Tekst = read[2];

                        komentar.Odobren = true;

                        if (!Database.komentariZaApartmane.ContainsKey(idKom))
                        {
                            Database.komentariZaApartmane.Add(idKom, komentar);
                        }
                    }
                     
                }

                sr.Close();
            }

            string pathSadrzaj = "~/App_Data/SadrzajApartmana.txt";
            pathSadrzaj = HostingEnvironment.MapPath(pathSadrzaj);
            Random idSadrzaja = new Random();
            // Random idAp = new Random();
            int idSadrzaj;

            if (File.Exists(pathSadrzaj))
            {
                StreamReader sr = new StreamReader(pathSadrzaj);

                while ((temp = sr.ReadLine()) != null)
                {
                    SadrzajApartmana sadrzaj = new SadrzajApartmana();

                    read = temp.Split('_');

                    if (read.Count() > 1)
                    {
                        idSadrzaj = idSadrzaja.Next();

                        sadrzaj.Naziv = read[1];

                       
                        if (!Database.sadrzajiApartmana.ContainsKey(idSadrzaj))
                        {
                            Database.sadrzajiApartmana.Add(idSadrzaj, sadrzaj);
                        }
                    }

                }

                sr.Close();
            }
        }

       
    }
}

